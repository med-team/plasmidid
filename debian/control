Source: plasmidid
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               help2man,
               ncbi-blast+ <!nocheck>,
               bedtools <!nocheck>,
               bowtie2 <!nocheck>,
               samtools <!nocheck>,
               prokka <!nocheck>,
               cd-hit <!nocheck>,
               mash <!nocheck>,
               circos <!nocheck>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/plasmidid
Vcs-Git: https://salsa.debian.org/med-team/plasmidid.git
Homepage: https://github.com/BU-ISCIII/plasmidID
Rules-Requires-Root: no

Package: plasmidid
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${python3:Depends},
         ncbi-blast+,
         bedtools,
         bowtie2,
         samtools,
         prokka,
         cd-hit,
         mash,
         circos
Recommends: trimmomatic,
            spades,
            python3-biopython,
            python3-numpy,
            python3-pandas
Description: mapping-based, assembly-assisted plasmid identification tool
 PlasmidID is a mapping-based, assembly-assisted plasmid identification
 tool that analyzes and gives graphic solution for plasmid
 identification.
 .
 PlasmidID is a computational pipeline that maps Illumina reads over
 plasmid database sequences. The k-mer filtered, most covered
 sequences are clustered by identity to avoid redundancy and the
 longest are used as scaffold for plasmid reconstruction. Reads are
 assembled and annotated by automatic and specific annotation. All
 information generated from mapping, assembly, annotation and local
 alignment analyses is gathered and accurately represented in a
 circular image which allow user to determine plasmidic composition in
 any bacterial sample.
